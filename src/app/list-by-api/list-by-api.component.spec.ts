import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListByApiComponent } from './list-by-api.component';

describe('ListByApiComponent', () => {
  let component: ListByApiComponent;
  let fixture: ComponentFixture<ListByApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListByApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListByApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
