import { Component, OnInit } from '@angular/core';
import { ListByApiService } from '../list-by-api.service';

@Component({
  selector: 'app-list-by-api',
  templateUrl: './list-by-api.component.html',
  styleUrls: ['./list-by-api.component.css']
})
export class ListByApiComponent implements OnInit {
  information: any[];
  constructor(private listbyapi: ListByApiService) { }

  ngOnInit(): void {
    this.information = this.listbyapi.getapidata();
  }

}
