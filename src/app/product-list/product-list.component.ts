import { Component, OnInit } from '@angular/core';
import { ListserviceService } from '../listservice.service';
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  pageTitle = 'Product List';
  imageWidth = 50;
  showimage = false;
  listfilter = 'cart';
  products: any[] = [];
  imagetoggle(): void {
    this.showimage = !this.showimage;
  }
  // tslint:disable-next-line:variable-name
  constructor(private listproduct: ListserviceService) { }
  ngOnInit() {
    this.products = this.listproduct.getProduct();
  }
}
