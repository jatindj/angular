import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductListComponent } from './product-list/product-list.component';
import { StarComponent } from './star/star.component';
import { ListserviceService } from './listservice.service';
import { ListInUlComponent } from './list-in-ul/list-in-ul.component';
import { HttpClientModule } from '@angular/common/http';
import { ListByApiComponent } from './list-by-api/list-by-api.component';
import { ListByApiService } from './list-by-api.service';

@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    StarComponent,
    ListInUlComponent,
    ListByApiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ListByApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
