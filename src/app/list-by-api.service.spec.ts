import { TestBed } from '@angular/core/testing';

import { ListByApiService } from './list-by-api.service';

describe('ListByApiService', () => {
  let service: ListByApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListByApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
