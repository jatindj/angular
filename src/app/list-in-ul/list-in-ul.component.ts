import { Component, OnInit } from '@angular/core';
import { ListserviceService } from '../listservice.service';

@Component({
  selector: 'app-list-in-ul',
  templateUrl: './list-in-ul.component.html',
  styleUrls: ['./list-in-ul.component.css']
})
export class ListInUlComponent implements OnInit {
  products: any[];
  constructor(private listui: ListserviceService) { }

  ngOnInit(): void {
    this.products = this.listui.getProduct();
  }

}
