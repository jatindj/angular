import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListInUlComponent } from './list-in-ul.component';

describe('ListInUlComponent', () => {
  let component: ListInUlComponent;
  let fixture: ComponentFixture<ListInUlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListInUlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListInUlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
